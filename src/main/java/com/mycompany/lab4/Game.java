/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Table table;
    private Player player1, player2;

    public Game() {
        player1 = new Player('O');
        player2 = new Player('X');
    }

    public void play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                saveStat();
                showTable();
                System.out.println(table.getCurrentPlayer().getSymbol()+"Win!!");
                break;
            }
            if(table.checkDraw()){
                saveDraw();
                showTable();
                System.out.println("Draw!!");
                break;
            }table.switchPlayer();
        }System.out.println("End Game");
    }
    
    
    private void saveStat() {
        if(table.getCurrentPlayer()==player1){
            player1.win();
            player2.lose();
        }else{
            player1.lose();
            player2.win();
        }
    }

    private void showWelcome() {
        System.out.println("Welcome to ox Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j]);
            }
            System.out.println("");
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input row col: ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table.setRowCol(row, col);
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol()+" Turn");
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
    }

}
